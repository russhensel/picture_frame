# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 16:18:43 2021

@author: russ



firest implementation, random dist over a range with a bias,
later have different methods


idea is to move ahaed in list, but in a semi-randoem way




"""


import random



class RandomIndex( object ):
    """
    About this class.....
    add the list and make a generator ?
    """
    #----------- init -----------
    def __init__(self, length, start = 0, width = 20, bias = 5  ):
        """
        See class doc
        andom_index.RandomIndex( len( file_as_list ), self.parameter.spicture_ran_width, self.parameter.picture_ran_bias  )
        """
        # this is the constructor run when you create
        # like  app = AppClass( 55 )
        self.length              = length
        self.ix_now              = start
        self.width               = 10
        self.bias                = 2

    # -----------------------------------
    def __repr__(self):
          return "App Class __repr__"
    # -----------------------------------
    # called by str( instance of AppClass )
    def __str__(self):
        a_str   = f"RandomIndex {1+1}"
        a_str   = f"{a_str}    width  = {self.width}"  #...
        a_str   = f"{a_str}    bias   = {self.bias}"  #...
        a_str   = f"{a_str}    more   = {1 * 1}"  #...
        return a_str

    # -----------------------------------
    def get_next(self):
        """
        what it says, read
        ?? very inefficient
        """
        min             = int( - self.width/2 + self.bias )
        max             = int( self.width/2 + self.bias )
        step            = random.randrange( min, max )
        self.ix_now     = self.ix_now + step
        self.ix_now     = self.ix_now % self.length
        return self.ix_now

    # -----------------------------------

