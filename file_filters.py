# -*- coding: utf-8 -*-
"""
Created on Sun Sep 12 10:15:23 2021

@author: russ

file_filters.py
file filters for directory exploration and backup and other uses of file selection

status:
    refactor from backup -- used in backup now, some working
    see tests for guide to use
    !! add abc ??

use:
    backup
    filefinder
    structured notes
    ????

Ideas

    add fnmatch  .... module
    use regular expressions

    Python fnmatch Module Examples.

Docs:

    Python File Filters - SageMath for Undergraduates
    https://sagemathnotes.miraheze.org/wiki/Python_File_Filters#Example_Setup



"""


import os
import time
import operator
import datetime
import pathlib
# import src_stats
import stat

# -----------  file filter objects  FF_ -------------

#  !! rename all to make sure described as filters   filter_for    xxx_ok_to_copy   .... ??
# !!make so filters can filter on file size and date



# really should start with abc or with protocol -- used abc

import abc
#import time
# from abc import ABC, abstractmethod

# ================= Class =======================
class FFabc( abc.ABC ):
    #__metaclass__ = abc.ABCMeta

    # ----------------------------------------
    def __init__( self, ):
        pass

    # ----------------------------------------

    # @abc.abstractmethod
    # def function_one( self,   ):
    #     pass
    #     #return




# ================= class AllFiles =========================
class FFAll( FFabc ):
    """
    this is a filter object which accepts all file names
    !! what if the file is a directory, they should not be see check_file
    """
    def __init__( self,  ):
        self.filter_name      = "FFAll"         # class name
        self.filter_function  = "accept all files"   # description of the function
        pass

    #-------------------------------------------
    def check_file( self, file_name, src_dir = None, dest_dir = None  ):
        """
        this is the call that actually 'does' the filtering
        for arguments look were called.
        """
        return True

    #-------------------------------------------
    def __str__(self):
        return self.filter_name   + " function = " + self.filter_function

# ================= class AllFiles =========================
class FFExtList( FFabc ):
    """
    this is a filter object which accepts all file names on a list of extensions.
    No dot in extension argument  sample:  filter_object  = directory_backup.FFExtList( ["bat", "py"]  )
    we lower case the extension use this when you create your list
    example

    filter_obj          = file_filters.FFExtList( ["txt", "py" ], include_true = True, use_lower = True )
    a_filter_function   = filter_obj.check_file

    """
    def __init__( self, list_of_extensions, include_true = True, use_lower = True ):
        self.filter_name          = "FFExtList"         # class name
        self.filter_description   = "accept based on list of extensions "   # description of the function
        self.list_of_extensions   = list_of_extensions  # better if a set
        self.include_true         = include_true
        self.use_lower            = use_lower

    #-------------------------------------------
    def check_file( self, file_name, src_dir = None, dest_dir = None  ):
        """
        this is the call that actually 'does' the filtering
        for arguments -- look were called.
        """
        splits     = file_name.split( "." )
        ext        = splits[-1]
        if self.use_lower:
            ext    = ext.lower()   # end one, could be several, fix case

        ok         = False
        if ext in self.list_of_extensions:
            ok = True

        if self.include_true:
            pass
        else:
            ok  = not ok

        msg = f"FFExtList check_file >>{file_name}<< >>{src_dir}<< >>{ok}<<"
        print( msg )

        return ok

    #-------------------------------------------
    def __str__(self):
        """
        !! needs more

        """
        # return self.filter_name   + " function = " + self.filter_function
        a_str   = f"{self.filter_name}       function_description =  {self.filter_description} "
        a_str   = f"{a_str}\n    list_of_extensions     = {self.list_of_extensions}"
        a_str   = f"{a_str}\n    use_lower              = {self.use_lower}"

        a_str   = f"{a_str}\n    include_true           = {self.include_true}"

        return a_str

# ================= class AllFiles =========================
class FFNameEquals( FFabc ):
    """
    equals, could inclue extension or not
    if not using extension !! what does basename eval to??
    """
    def __init__( self, list_of_equals = None, include_true = True, use_lower = True, use_extension = True ):
        self.filter_name          = "FFNameEquals"         # class name
        # self.filter_function      = "see name"   # description of the function --- removed
        self.filter_description   = "see name"
        if list_of_equals == None:
            self.list_of_equals = []
        else:
            self.list_of_equals  = list_of_equals  # better if a set

        self.include_true         = include_true
        self.use_lower            = use_lower
        self.use_extension        = use_extension

    #-------------------------------------------
    def check_file( self, file_name, src_dir = None, dest_dir = None  ):
        """
        this is the call that actually 'does' the filtering
        for arguments -- look were called.
        we need to get full path and then shorten it down...
        does the file need to exist
        !! add include path ??

        """
        check_name        = file_name
        check_name        = os.path.basename( check_name )    # path is gone, extension still there

        if self.use_lower:
            check_name    = check_name.lower()

        if not self.use_extension:
            splits         = check_name.rsplit( "." )   # ok for multiple "." ??
            check_name     = splits[0]
        print( f"check name = { check_name }")
        equals_to = False
        for i_string in self.list_of_equals:
            if self.use_extension:
                if i_string == check_name:
                    equals_to = True
                    break
            else:
                basename = os.path.basename( i_string )
                if basename  == check_name:
                    equals_to = True
                    break

        #  is this logic correct for both must match and cannot match, check this and the extension filter
        # ?? can compute this ??
        if self.include_true:
            ret = equals_to
        else:
            ret = not equals_to

        return ret

   #-------------------------------------------
    def __str__(self):

        a_str   = f"{self.filter_name}       function_description =  {self.filter_description} "
        a_str   = f"{a_str}\n    list_of_equals         = {self.list_of_equals}"
        a_str   = f"{a_str}\n    use_extension          = {self.use_extension}"
        a_str   = f"{a_str}\n    self.include_true      = {self.include_true}"
        a_str   = f"{a_str}\n    self.use_lower         = {self.use_lower}"
        return a_str

# ================= class AllFiles =========================
class FFNameStartsWith( FFabc ):
    """

    """
    def __init__( self, list_of_starts_with = None, include_true = True, use_lower = True ):
        self.filter_name          = "FFNameStartsWith"         # class name
        # self.filter_function      = "see name"   # description of the function --- removed
        self.filter_description   = "see name"
        if list_of_starts_with == None:
            self.list_of_starts_with = []
        else:
            self.list_of_starts_with  = list_of_starts_with  # better if a set

        self.include_true         = include_true
        self.use_lower            = use_lower

    #-------------------------------------------
    def check_file( self, file_name, src_dir = None, dest_dir = None  ):
        """
        this is the call that actually 'does' the filtering
        for arguments -- look were called.
        we need to get full path and then shorten it down...
        does the file need to exist

        """
        basename = os.path.basename( file_name )    # !! is the file name the whole thing, does it include src_dir if no can this work?

        if self.use_lower:
            basename    = basename.lower()

        starts_with = False
        for i_string in self.list_of_starts_with:
            if basename.startswith( i_string ):
                starts_with = True
                break

        if self.include_true:
            ret = starts_with
        else:
            ret = not starts_with

        return ret

    #-------------------------------------------
    def __str__(self):

        a_str   = f"{self.filter_name}       function_description =  {self.filter_description} "
        a_str   = f"{a_str}\n    list_of_starts_with    = {self.list_of_starts_with}"
        a_str   = f"{a_str}\n    self.include_true      = {self.include_true}"
        a_str   = f"{a_str}\n    self.use_lower         = {self.use_lower}"
        return a_str


# ================= class =========================
class FFSize( FFabc ):
    """
    This is a filter object which excludes/includes based on size  large files
    based on what criteria -- lets rebuild file name and get all file data
    """
    def __init__( self,  size,  compare_true_operator     ):
        self.filter_name            = "FFSize"
        self.filter_function        = f"exclude large files based on size = {size}"
        self.size                   = size
        self.compare_true_operator  = compare_true_operator

    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):
        """
        this function checks a file name, in this one
        Args: same as other FF
        doe we combine src_dir with file_name if present ?!!

        """
        print( "!! unfinished/tested")
        src_stats          = os.stat( file_name )
        msg    = f"in check_file src_stats {src_stats}"
        print( msg )
        file_size          = src_stats[src_stats.ST_SIZE]

        a_check    = self.compare_true_operator( file_size, self.size )

        return a_check

    #-------------------------------------------
    def __str__(self):
        return self.filter_name   + " function = " + self.filter_function

# ================= class =========================
class FFExcludeAll( FFabc ):
    """
    This is a filter object which excludes all files.  For debugging  --- but mostly make no sense

    """
    def __init__( self,  ):
        self.filter_name      = "FFExcludeAll"
        #self.filter_function  = "exclude all files, give debug info "
        self.filter_description   = "exclude all files, give debug info "

    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):
        """
        this function checks a file name, in this one
        Args:
            file_name  full file name with path: D:\Russ\0000\python00\python3\_projects\backup\TestSource\Sub0\Sub2\file2_2.txt
            src_dir    all source      path up to be not including final /
                D:\Russ\0000\python00\python3\_projects\backup\TestSource\Sub0\Sub2
            dest_dir   all destination path up to be not including final /
                D:\Russ\0000\python00\python3\_projects\backup\TestDest\TestSource\Sub0\Sub2<

        note we may have mixed /  and \
        """
        msg    = f"in check_file file_name {file_name} \n    src_dir >{src_dir}<\n    dest_dir >{dest_dir}<"
        print( msg )

        src_stats          = os.stat( file_name )
        msg    = f"in check_file src_stats {src_stats}"
        print( msg )

        msg    = (     f"file name:             {file_name} \n" +
                        f"Size in bytes:        {src_stats[stat.ST_SIZE]} \n" +
                        f"Last access:          {time.ctime( src_stats[stat.ST_ATIME] ) } \n" +
                        f"Last modification:    {time.ctime( src_stats[stat.ST_MTIME] ) } \n" +
                        f"Last status:          {time.ctime( src_stats[stat.ST_CTIME] ) } \n" +
                        f"User id of the owner: {src_stats[stat.ST_UID]} \n" +
                        f"Group id of the owner:{src_stats[stat.ST_GID ]} \n" +

                        ""
                )

        print( msg )

        splits = file_name.split( "." )
        ext    = splits[-1].lower()   # end one, could be several??, fix case !!
        msg    = f"in check_file ext {ext}"
        print( msg )

        return False

    #-------------------------------------------
    def __str__(self):
        return self.filter_name   + " function = " + self.filter_function

# ================= class =========================
class FFADate( FFabc ):
    """
    !! looks like not done
    this is a filter object which accepts files with a single extension
    extension is not case sensitive
    for windows may be ok on others
    """
    def __init__( self, date_string = None ):
        self.filter_name      = "FFADate"
        self.filter_function  = "error extension not set"
        self.timestamp        = None
        if date_string is None:
            self.date_string      = ""
        else:
            self. date.string           = date_string

        self.relation         = operator.lt    # default can be set file < adate        #  <, < = !=    import operator f_op    = operator.lt
        self.set_date( date_string )

    #-------------------------------------------
    def set_date( self, date_string ):

        a_format              = "%d/%m/%Y"
        self.timestamp        = time.mktime( datetime.datetime.strptime( date_string, a_format ).timetuple() )
        self.date_string      = date_string
        self.set_filter_function()

    #-------------------------------------------
    def set_relation( self, a_relation ):
        # looks like wip not working
        self.relation         =  a_relation
        self.set_filter_function()

    #-------------------------------------------
    def set_filter_function( self, ):
        # looks like wip
        self.filter_function  = "file date " + " relation " + str( self.relation ) + self.date_string + " files only"

    #-------------------------------------------
#    def check_file( self, file_name, src_dir, dest_dir  ):
#        return True

#    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):
        """
        """
        # import os
        stats = os.stat( file_name )
        #print "stats are",file1,stats[8]
        modified = stats[8]
        print("in check file")
        a_check    = self.relation( modified, self.timestamp )
        #return True
        print( "Adate check " + file_name + " " + str( a_check ), flush = True )

        #return ( self.relation( modified, self.timestamp ) )
        return ( a_check )

    #-------------------------------------------
    def __str__(self):

        if self.timestamp == None:
            self.filter_function  = "error extension not set"

        ret  = self.filter_name   + " function = " + self.filter_function
        return ret

# ================= class =========================
class FFMultipleFilters( FFabc ):
    """
    !!   mostly done

    see test

    """
    #-------------------------------------------
    def __init__( self,   ):
        self.filter_name         = "FFMultipleFilters"
        self.filter_description  = "combine multiple filters"
        self.filter_object_list  = []

    #-------------------------------------------
    def _check_file_implemented( self, file_name, src_dir, dest_dir  ):
        """
        this function needs to be replaced, my standard way of doing this
        seems to result in a function call without replace so we do it as in
        ?? can we change to a property
        """
        print( f"***check_file_implemented internal stub should be replaced  >>{self}<<, \n    >>{file_name}<<, \n    >>{src_dir}<<", flush = True)
        pass

    #-------------------------------------------
    def set_check_file( self, check_file_function  ):
        """
        what it says, read
        ?? replace with a property
        """
        self._check_file_implemented  = check_file_function

    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):
        """
        needs to be set from outside ...
        could consider call by key word
        see  check_file_implemented
        """
        print( f"check_file internal >>{self}<<, \n    >>{file_name}<<, \n    >>{src_dir}<<", flush = True)

        # this odd call seems to be required by the injection of the function externally
        self._check_file_implemented( self = self, file_name = file_name, src_dir = src_dir, dest_dir = dest_dir )
        #self.check_file_implemented(  file_name = file_name, src_dir = src_dir, dest_dir = dest_dir )
        return True


        1/0
        msg    = f"in check_file file_name {file_name} \n    src_dir >{src_dir}<\n    dest_dir >{dest_dir}<"

    #-------------------------------------------
    def __str__(self):

        a_str   = f"{self.filter_name}       function_description =  {self.filter_description} "
        # a_str   = f"{a_str}\n    list_of_starts_with    = {self.list_of_starts_with}"
        # a_str   = f"{a_str}\n    self.include_true      = {self.include_true}"
        # a_str   = f"{a_str}\n    self.use_lower         = {self.use_lower}"
        return a_str


# ---- Directories =========================
class DFAll( ):
    """
    this is a filter object which accepts all directory names
    leaving in dest_dir for now

    """
    def __init__( self,  ):
        self.filter_name      = "DFAll"         # class name
        self.filter_description  = "accept all directories "   # description of the function
        pass

    #-------------------------------------------
    def check_dir( self,   src_dir = None, dest_dir = None  ):
        """
        this is the call that actually 'does' the filtering
        for arguments look were called.
        """
        msg = f"DFAll check_dir {src_dir}"
        print( msg )
        return True

    #-------------------------------------------
    def __str__(self):
        return f"{self.filter_name}       function_description =  {self.filter_description} "

#-------------------------------------------
class DFNameStartsWith(  ):
    """
    directory filter -- very similar to file filter but think needs seperate code
    !! need to strip off the dire id like D:   may be a pathlib, or look for :

    """
    def __init__( self, list_of_starts_with = None, include_true = True, use_lower = True ):
        self.filter_name          = "DFNameStartsWith"         # class name
        self.filter_description   = "see name"     # description of the function

        if list_of_starts_with == None:
            self.list_of_starts_with = []
        else:
            self.list_of_starts_with  = list_of_starts_with  # better if a set

        self.include_true         = include_true
        self.use_lower            = use_lower

    #-------------------------------------------
    def check_dir( self, src_dir = None, dest_dir = None  ):
        """
        this is the call that actually 'does' the filtering
        for arguments -- look were called.

        """
        final_name         = pathlib.PureWindowsPath( src_dir ).parts[-1]

        if self.use_lower:
            final_name    = final_name.lower()

        starts_with = False
        for i_string in self.list_of_starts_with:
            if final_name.startswith( i_string ):
                starts_with = True
                break

        if self.include_true:
            ret = starts_with
        else:
            ret = not starts_with

        return ret

    #-------------------------------------------
    def __str__(self):
        a_str   = f"{self.filter_name}       function_description =  {self.filter_description} "
        a_str   = f"{a_str}\n    list_of_starts_with    = {self.list_of_starts_with}"
        a_str   = f"{a_str}\n    self.include_true      = {self.include_true}"
        a_str   = f"{a_str}\n    self.use_lower         = {self.use_lower}"

        return a_str


# -----------  End Dir Filters   -------------

