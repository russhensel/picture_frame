#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Variouse classes to extend tkinter funcionality
     browsers
     message frames
     and styling of widgets



make a title property to set the title on the fly??

master in rsh_lib, gui_ext

us sys path for development, then copy over file and edit for git hub




"""


import tkinter as Tk
from   tkinter.filedialog import askopenfilename
from   tkinter.filedialog import askdirectory
#import  sys
from   tkinter import ttk
from   tkinter.scrolledtext import ScrolledText
import pyperclip

from   app_global import AppGlobal


# ----------------------------------------
def test_callback( a_key ):
    """
    get the string on the clicked line

    """
    # print( f"test....test...test {event} ")
    # a_key   = event.widget.selection_get()
    print( a_key )
    return a_key

# ----------------------------------------
def test_old( event ):
    """
    get the string on the clicked line??


    """
    print( f"test....test...test {event} ")
    a_key   = event.widget.selection_get()
    print( a_key )
    return a_key

# -----------------------------------------
class FileBrowseWidget( Tk.Frame ):
    """
    let user pick a file name on their computer
    not sure why making it into a widget is a good idea but here goes
    this is a widget that both holds a filename
    and lets you browse to a file
    how is it different from just a couple of widgets
    in your frame ... more reusable ?
    better looking or what
    see graph_smart_plug gui for a use
    !! code is duplicated in 2 guis, this should be fixed

    Tkinter Dialogs — Python 3.9.6 documentation
    https://docs.python.org/3/library/dialog.html#module-tkinter.filedialog

    need an example interface to this

    build it and keep a reference
        a_bw     = FileBrowseWidget( a_parent )
    get the text from it
        a_bw.get_text()     and set_text()

    use some instance variables to set browse behaviour
        a_bw.initialdir    = "./",
        a_bw.title         = "Select file for db",
        a_bw.filetypes     = (("database files","*.db"),("all files","*.*"))

    """
    def __init__(self, parent ):
        #super( BrowseWidget, self ).__init__( parent, width=60, height=20, bg ="red")
        super(  ).__init__( parent, width=60, height=20, bg ="red")

        a_widget      = Tk.Label( self, text="Get File Name:", anchor = "e")
        a_widget.grid( row = 1, column = 0, sticky = Tk.N + Tk.S )
        AppGlobal.gui_style.style_widget( a_widget )
        a_widget.config( width  = 35 )

        self.a_string_var = Tk.StringVar()

        self.entry_1      = Tk.Entry( self , width=100,   text = "bound", textvariable = self.a_string_var )
        AppGlobal.gui_style.style_widget( self.entry_1 )
        self.entry_1.grid( row=1, column=1 , sticky = Tk.N + Tk.S )

        self.button_2     = Tk.Button( self, text = "Browse...", command = self.browse )
        AppGlobal.gui_style.style_widget(  self.button_2 )
        self.button_2.grid( row=1, column=3 )

        self.initialdir    = "./",
        self.title         = "Select file for db",
        self.filetypes     = (("database files","*.db"),("all files","*.*"))

    # ------------------------------------------
    def browse( self ):
        """
        browse for a file name
        return full path or "" if no file chosen
        """
        Tk.Tk().withdraw()
        filename     = askopenfilename(  initialdir   = self.initialdir,
                                         title        = self.title ,
                                         filetypes    = self.filetypes )

        if filename == "":
            return

        self.set_text( filename )
        print( f"get_text = {self.get_text()}", flush = True )

    # ------------------------------------------
    def set_text( self, a_string ):
        """
        get the text from the entry
        """
        self.a_string_var.set( a_string )

    # ------------------------------------------
    def get_text( self, ):
        """
        get the text from the entry
        """
        a_string   =  self.a_string_var.get(  )
        return( a_string )

# -----------------------------------------
class DirBrowseWidget( Tk.Frame ):
    """
    let user pick a directory name on their computer
    Tkinter Dialogs — Python 3.9.6 documentation
    https://docs.python.org/3/library/dialog.html#module-tkinter.filedialog
    """
    def __init__(self, parent ):
        #super( BrowseWidget, self ).__init__( parent, width=60, height=20, bg ="red")
        super(  ).__init__( parent, width=60, height = 20, bg ="red")
        a_widget      = Tk.Label( self, text="Get Directory Name:", anchor = "e" )
        AppGlobal.gui_style.style_widget(  a_widget )
        a_widget.grid( row = 1, column = 0, sticky = Tk.N + Tk.S )
        a_widget.config( width  = 35 )

        self.a_string_var = Tk.StringVar()

        self.entry_1      = Tk.Entry( self , width = 100,   text = "bound", textvariable = self.a_string_var ) #  height = 20 ng
        AppGlobal.gui_style.style_widget( self.entry_1 )
        self.entry_1.grid( row = 1, column = 1, sticky = Tk.N + Tk.S )

        self.button_2 = Tk.Button( self, text = "Browse...", command = self.browse )
        AppGlobal.gui_style.style_widget(  self.button_2 )
        self.button_2.grid( row = 1, column = 3 )

        # set these as part of interace or make a function

    # ------------------------------------------
    def browse( self ):
        """
        browse for a file name
        return full path or "" if no file chosen
        """
        Tk.Tk().withdraw()
        dirname   = askdirectory()

        self.set_text( dirname )
        print( f"get_text = {self.get_text()}", flush = True )


    # ------------------------------------------
    def set_text( self, a_string ):
        """
        get the text from the entry
        """
        self.a_string_var.set( a_string )

    # ------------------------------------------
    def get_text( self, ):
        """
        get the text from the entry
        """
        a_string   =  self.a_string_var.get(  )
        return( a_string )

# ----------------------------------------
class MessageFrame( Tk.Frame ):
    """
    message frame used in so many apps

        a_frame            = gui_ext.MessageFrame( parent,  )
        self.message_frame = a_frame
        return a_frame

    """
    def __init__(self, parent ):
        #super( BrowseWidget, self ).__init__( parent, width=60, height=20, bg ="red")
        super(  ).__init__( parent, width = 60, height = 20, bg ="yellow")
        a_frame        = self._make_message_frame( parent,  )
        self.max_lines =  10000      # part of interface
        # a_frame.pack()
        # parameters !!!
        # self.default_scroll     = 1        # 1 auto scroll the receive area, else 0
        # self.max_lines          = 1000

    # ------------------------------------------
    def _make_message_frame( self, parent, default_scroll = True  ):
        """
        default_scroll is new
        make the message frame
        return of i_frame is just self
        """
#        color   = "black"   # this may need a bit of rework -- looks like not used
        #iframe  = Tk.Frame( parent, width=300, height=800, bg ="blue", relief = Tk.RAISED, borderwidth=1,  )
        iframe  = self

        # bframe is for the buttons on the left
        bframe  = Tk.Frame( iframe, bg = "white", width=30  ) # width=300, height=800, bg ="blue", relief=RAISED, borderwidth=1,  )
        bframe.grid( row=0, column=0, sticky = Tk.N + Tk.S )

        text0 = Tk.Text( iframe , width=50, height=20 )

        s_text0 = Tk.Scrollbar( iframe  )
        s_text0.grid( row=0, column=2, sticky = Tk.N + Tk.S )

        s_text0.config( command=text0.yview )
        text0.config( yscrollcommand=s_text0.set )

        text0.grid( row=0, column=1, sticky = Tk.N + Tk.S + Tk.E + Tk.W  )

        self.rec_text  = text0

        iframe.grid_columnconfigure( 1, weight=1 )
        iframe.grid_rowconfigure(    0, weight=1 )

        # now into the button frame bframe

        # spacer
        s_frame = Tk.Frame( bframe, bg ="green", height=20 ) # width=30  )
        s_frame.grid( row=0, column=0  )
        row_ix   = 0

        # --------------------
        b_clear = Tk.Button( bframe , width=10, height=2, text = "Clear" )
        b_clear.bind( "<Button-1>", self.do_clear_button )
        b_clear.grid( row=row_ix, column=0   )
        row_ix   += 1

        #-----
        b_temp = Tk.Button( bframe , width=10, height=2, text = "Copy Selection"   )
        # b_temp.bind( "<Button-1>", self.doButtonText )
        b_temp.grid( row=row_ix, column=0   )
        row_ix   += 1

        #-----
        b_copy = Tk.Button( bframe , width=10, height=2, text =  "Copy All" )
        b_copy.bind( "<Button-1>", self.do_copy_button )
        b_copy.grid( row=row_ix, column=0   )
        row_ix += 1

        # -------------
        self.cb_scroll_var  = Tk.IntVar()  # for check box in reciev frame
        a_widget = Tk.Checkbutton( bframe,  width=7, height=2, text="A Scroll", variable=self.cb_scroll_var,  command=self.do_auto_scroll )
        a_widget.grid( row=row_ix, column=0   )
        row_ix += 1
        self.cb_scroll_var.set( default_scroll ) # was AppGlobal.parameters.default_scroll )

        return iframe

    # ------------------------------------------
    def do_auto_scroll( self, auto = True  ):
        """
        still configured so needed, but will not work ??
        pass, not needed, place holder see
        """
        print( "do_auto_scroll fix !!" )

        return

    # ------------------------------------------
    def do_clear_button( self, event):
        """
        for the clear button
        clear the message area
        """
        self.clear_message_area()

    # ------------------------------------------
    def clear_message_area( self,   ):
        """
        what it says.....message area sometimes rec or send
        """
        self.rec_text.delete( 1.0, Tk.END )


    # ------------------------------------------
    def do_copy_button( self, event ):
        """
        copy all text to the clipboard
        """
        data  = self.rec_text.get( 1.0, Tk.END )
        pyperclip.copy( data )
        return

    # ---------------  end of button actions and class

    # ---------------------------------------
    def display_string( self, a_string, update_now = False ):
        """
        print to message area, with scrolling and
        log if we are configured for it

        parameters.gui_text_log_fn    = False  # "gui_text.log"       # a file name or something false


        parameters.log_gui_text       = False # True or false to log text
        parameters.log_gui_text_level = 10    # logging level for above

        """
        #rint( "debug for display_string")
        if  AppGlobal.parameters.gui_text_log_fn:
            # for now open close.... later perhaps improve
            with open( AppGlobal.parameters.gui_text_log_fn, "a"  ) as f :
                f.write( a_string )   # do we need \n check
                #rint(   a_string )

        if  AppGlobal.parameters.log_gui_text:
            AppGlobal.logger.log( AppGlobal.parameters.log_gui_text_level, a_string, )

        self.rec_text.insert( Tk.END, a_string, )      # this is going wrong, why how
        try:
             numlines = int( self.rec_text.index( 'end - 1 line' ).split('.')[0] )  # !! beware int( None ) how could it happen ?? it did this is new
        except Exception as exception:
        # Catch the custom exception
            self.logger.error( str( exception ) )
            print( exception )
            numlines = 0
        if numlines > self.max_lines:
            cut  = int( numlines/2  )    # lines to keep/remove
            self.rec_text.delete( 1.0, str( cut ) + ".0" )               # remove excess text
#            msg     = "Delete from test area at " + str( cut )
#            self.logger.info( msg )

        if self.cb_scroll_var.get():
            self.rec_text.see( Tk.END )

        if update_now:
            self.root.update()
            print( "!! self.root not valid here ")
        return

# ----------------------------------------
class ListboxScroll( object ):
    """
    scrolling listbox with title and utility functions
    master in rsh_lib, gui_ext

    consider making descandant of Frame

    note that self.   hold components for possible use

    so far single selection

    """
    def __init__( self, parent_frame, a_title = "a title", width = None, height = None ):
        """
        if title is None do not construct that part

        """
        self.version        = "2021_08_04"
        self.parent         = parent_frame
        self.title          = a_title
        self.click_function = None     # set later or externally
        self.frame          = None     # the frame this is in use xxx.frame
        self.listbox        = None     # the listbox
        self.outer_frame    = None

        if width is None:
            width = 100

        if height is None:
            height = 100

        self._make_titled_listbox_( width, height )

    # ----------------------------------------
    """
    functions needed
    set command  -- just a property ... no needs function could make a propeety @
    inset_row
    """
    # ----------------------------------------
    def insert_row( self, value ):
        """
        what is says
        """
        self.listbox.insert(  Tk.END, value )

     # ----------------------------------------
    def clear_rows( self,  ):
        """
        what is says
        """
        self.listbox.delete(0, Tk.END)

    # ----------------------------------------
    def set_values( self, values ):
        """
        what is says
        can we use to clear set list without using insert row
        """
        #self.listbox.configure( values )
        # clear
        #for

    # ----------------------------------------
    def set_width( self, width ):
        """
        what is says -- read
        """
        # label seems to be the controlling thing
        self.label_widget.configure( width = width )

    # ----------------------------------------
    def set_height( self, height ):
        """
        what is says -- read
        """
        # label seems to be the controlling thing
        self.listbox.configure( height = height )
        print( "!! implement set_height if not working" )

    # ----------------------------------------
    def set_click_function( self, a_function ):
        """
        what is says -- would bind be better
        we want an index or contents of row or both need
        more work here
        """
        self.click_function =  a_function

    # ----------------------------------------
    def _click_function( self, event ):
        """
        what is says -- would bind be better -- on the list box?, search
        we want an index or contents of row or both need
        more work here
        """
        if self.click_function is None:
            print( "ListboxScroll -- click_function not set" )
        else:
           # sending the selection get, but perhaps should send the event and let click function ....!!!
           # a_key   = event.widget.selection_get()
           #rint( a_key )
           # self.click_function( a_key )
           self.click_function( event )

    def get_selected_ix( self ):
        """
        return 0 based selection -1 if nothing
        assumes in single select mode
        """
        selected_ix   = self.listbox.curselection()

        if selected_ix == tuple(  ):
           selected_ix  = -1
        else:
            selected_ix  =  selected_ix[0]   # since we allow only 1 selection

        return selected_ix

    # ----------------------------------------
    def _make_titled_listbox_( self, width, height  ):
        """
        for snips and snippets?
        return ( famelike_thing, listbox_thing)  ?? make a class, better access to components
        """
        a_frame      = Tk.Frame( self.parent, width = 1000 )

        a_listbox    = Tk.Listbox( a_frame, height = 5 )
        self.listbox  = a_listbox
        a_listbox.grid( column = 0, row = 1, sticky=(Tk.N, Tk.W, Tk.E, Tk.S) )

        a_listbox.bind( "<<ListboxSelect>>", self._click_function  )

        s = Tk.Scrollbar( a_frame, orient=Tk.VERTICAL, command=a_listbox.yview)
        s.grid(column=1, row=1, sticky=(Tk.N, Tk.S))
        a_listbox[ 'yscrollcommand'] = s.set

        a_label = Tk.Label( a_frame, text = self.title, width = width )
        a_label.grid( column = 0, row = 0, sticky=( Tk.N, Tk.E, Tk.W) )
        self.label_widget  = a_label

        #  ttk.Sizegrip().grid(column=1, row=1, sticky=(Tk.S, Tk.E)) size grip not appropriate here
        a_frame.grid_columnconfigure( 0, weight=1 )
        a_frame.grid_rowconfigure(    0, weight=0 )
        a_frame.grid_rowconfigure(    1, weight=1 )

        # for overall widget
        self.set_width( width  )
        self.set_height( height )

        self.listbox     = a_listbox
        self.frame       = a_frame
        self.outer_frame = a_frame

        return a_frame

# -----------------------------------
class PlaceInGrid( object ):
    """
    called sequentially to help layout grids in a row and column format
    columnspan=2, rowspan=2
    add columnspan to place  make it increment in direction we are moving ....??

    to do
    add column span row span -- keep delta ? delta is span in direction, but may need both ?
    add setup for stickyness ??


    placer    = gui_ext.PlaceInGrid( 99,  by_rows = True )
    placer.place(  a_widget, columnspan = None,   rowspan = None, sticky = None )

    """
    def __init__( self,  max, by_rows  ):

        self.max      = max
        self.ix_row   =  0
        self.ix_col   =  0
        self.sticky = Tk.W + Tk.E  + Tk.N + Tk.S
        self.by_rows  = by_rows
        if by_rows:
            self.function =  self._place_down_row_
        else:
            self.function =  self._place_across_col_

    # -----------------------------------
    def place( self, a_widget, columnspan = None,   rowspan = None, sticky = None ):
        """
        move row or column by delta grid spacings after pacing control
        what is row span vs deltac
        """
        if columnspan is None:
            columnspan = 1

        if rowspan is None:
            rowspan    = 1

        #app_global.print_debug( f"row,co = {self.ix_row}, {self.ix_col}" )
        self.function( a_widget,  columnspan = columnspan, rowspan = rowspan, sticky = sticky )

    # -----------------------------------
    def _place_down_row_( self, a_widget, columnspan, rowspan, sticky = None ):
        """
        one of the value intended for self.function
        does its name
        not much tested
        need to add sticky
        """
        print( "still need to make sticky stick !!")
        if sticky is None:
            sticky = self.sticky
        #rint( f"_place_down_row_ row = {self.ix_row} col = {self.ix_col}"  )
        a_widget.grid( row = self.ix_row, column = self.ix_col, rowspan = rowspan   )

        self.ix_row += rowspan
        if self.ix_row >= self.max:
            print( f"hit max row {self.max}"  )
            self.ix_col += 1
            self.ix_row = 0

    # -----------------------------------
#    delta_row_col( delta_row, delta_col )
#    add a span argument

    # -----------------------------------
    def new_row( self, delta_row = 1,  ):
        """
        start a new row in col 1
        !! also for col
        """
        self.ix_row     += delta_row
        self.ix_col      = 0

    # -----------------------------------
    def set_row( self, row,  ):
        """
        what if beyond max
        """
        self.ix_row = row

    # -----------------------------------
    def set_col( self,  col ):
        """
        what it says, why not just the property

        """
        self.ix_col = col

    # -----------------------------------
    def _place_across_col_( self, a_widget, *, columnspan,  rowspan, sticky, ):
        """
        what it says
        one of the value intended for self.function
        args:
            widget    -> the widget being placed
            columnspan    -> the column span
            rowspan    -> the rowspan
            sticky    -> temporary override of sticky via argument
        """
#        print( f"_place_across_col_ row = {self.ix_row} col = {self.ix_col}"  )
        # defaulting should be done in place
        # if columnspan is None:
        #     columnspan = 1

        # if rowspan is None:
        #     rowspan = 1

        # if sticky is None:
        #     sticky = self.sticky

        print( f"_place_across_col_ ({self.ix_col}, {self.ix_row})  columnspan = {columnspan}")

        a_widget.grid( row = self.ix_row, column = self.ix_col, columnspan = columnspan, rowspan  = rowspan, sticky = sticky  )   # sticky = Tk.W + Tk.E  + Tk.N + Tk.S

        self.ix_col += columnspan
        if self.ix_col >= self.max:
            print( f"hit max row {self.max}"  )
            self.ix_row += 1
            self.ix_col  = 0

        #print("_place_across_col_",  self.ix_row, self.ix_col  )

# -*- coding: utf-8 -*-
"""
GUI utilities, right now for gui build later ?



module or object or mix,

i tend to go with objects so we will do an
instantiated object


import gui_util

self.a_gui_util    = gui_util.GuiUtil()
self.a_gui_util.style_button( a_widget )

"""

import  tkinter   as Tk
from    app_global import AppGlobal
# ======================= begin class ====================
class GuiStyle( object ):
    """
    for consistent styles in the gui from structured notes now in rhsutil master and copy to each project
    gui utilities for the application
    another AppGlobal thing
    ?? make different styles
    still need 'tkinter.Checkbutton'>
     str>><class 'tkscrolledframe.widget.ScrolledFrame'><<   repr >><tkscrolledframe.widget.ScrolledFrame object .!

    """
    def __init__( self, ):
        """
        register with AppGlobal so this is a requirement


        """

        AppGlobal.gui_style  = self   #!! not int AppGlobal but use as a_gui_util

        self.widget_style_dispactch_dict   = {}

        #rint( f"init GuiStyle" )
        self._set_style_values()

    # ------------------------------------------
    def make_styles_from_parameters( self, a_parameter = None ):
        pass  # ?? and idea

    # ------------------------------------------
    def make_styles_from_widget( self, a_widget ):
        """
        ?? an idea
        """
        pass

    # ------------------------------------------
    def _set_style_values( self,    ):
        """
        style values determine the color... values of items built
        this is just a beginning
        btn   or button  or bn

        https://stackoverflow.com/questions/4969543/colour-chart-for-tkinter-and-tix/6932500
        http://tephra.smith.edu/dftwiki/index.php/Color_Charts_for_TKinter
        bg  background
        fg  foreground

        """

        # --------- gui standards work on more have themes have in another object ?
        # but there are things called themes, look for more doc
        # self.btn_color     = self.parameters.btn_color
        # self.bkg_color     = self.parameters.bkg_color
        # #self.text_color    = "white"

        # ---- button
        self.tk_button_bg  = "gray"                             # button background color
        self.tk_button_bg  = "linen"
        self.tk_button_fg  = "black"                            # button foreground color nothing on win10
        self.tk_button_ab  = "lightgray"
        self.tk_button_relief  =  Tk.GROOVE          #
        self.tk_button_relief  =  Tk.SUNKEN         #   SUNKEN   RAISED

        """
         )      # padx = 2, pady = 2,
        relief     = None,
        Tk.GROOVE,


        None,
        Tk.RAISED
        Tk.RIDGE,
        "solid",
        relief=FLAT )
        B2 = Tkinter.Button(top, text ="RAISED", relief=RAISED )
        B3 = Tkinter.Button(top, text ="SUNKEN", relief=SUNKEN )
        B4 = Tkinter.Button(top, text ="GROOVE", relief=GROOVE )
        B5 = Tkinter.Button(top, text ="RIDGE", relief=RIDGE )
        """

        self.tk_button_borderwidth  =  5
                            #
        #self.tk_button_

        self.button_dict      = {  "bg":               self.tk_button_bg,
                                   "fg":               self.tk_button_fg,
                                   "activebackground": self.tk_button_ab,
                                   "relief":           self.tk_button_relief,
                                   "borderwidth":      5,
                                }

        self.widget_style_dispactch_dict[ Tk.Label ] = self.button_dict   # is ok if we dispatch on type, by not instance

        # ---- checkbutton
        self.tk_checkbutton_bg            = "gray"   #
        self.tk_checkbutton_fg            = "blue"
        self.tk_checkbutton_ab            = "green"
        self.tk_checkbutton_borderwidth   = 5,
        self.tk_checkbutton_relief        = Tk.RAISED,

        self.checkbutton_dict   = {  "bg":               self.tk_checkbutton_bg,
                               "relief":           self.tk_checkbutton_relief,
                               "borderwidth":      self.tk_checkbutton_borderwidth,
                                 # "fg":               self.tk_frame_bg,    # not a known option win10
                                 # "activebackground": self.tk_frame_bg,    # not a known option win10
                                }


        # ---- frame
        self.tk_frame_bg            = "gray"   #
        self.tk_frame_fg            = "blue"
        self.tk_frame_ab            = "green"
        self.tk_frame_borderwidth   = 5,
        self.tk_frame_relief        = Tk.RAISED,

        self.frame_dict   = {  "bg":               self.tk_frame_bg,
                               "relief":           self.tk_frame_relief,
                               "borderwidth":      self.tk_frame_borderwidth,
                                 # "fg":               self.tk_frame_bg,    # not a known option win10
                                 # "activebackground": self.tk_frame_bg,    # not a known option win10
                                }

        # ---- tab pages tabp
        self.tk_tabp_bg  = "red"
        self.tk_tabp_fg  = "blue"
        self.tk_tabp_ab  = "green"

        self.tabp_dict   = {  "bg":               self.tk_tabp_bg,
                                 # "fg":               self.tk_tabp_bg,    # not a known option win10
                                 # "activebackground": self.tk_tabp_bg,    # not a known option win10
                                }

        # ---- label
        self.tk_label_bg  = "red"
        self.tk_label_bg  = "linen"
        self.tk_label_fg  = "blue"
        self.tk_label_ab  = "green"

        self.label_dict   = {  "bg":               self.tk_label_bg,
                               "borderwidth":      5,
                                 # "fg":               self.tk_tabp_bg,    # not a known option win10
                                 # "activebackground": self.tk_tabp_bg,    # not a known option win10
                                }

        # ---- checkbox
        self.tk_checkbox_bg  = "red"
        self.tk_checkbox_fg  = "blue"
        self.tk_checkbox_ab  = "green"

        self.checkbox_dict   = {  "bg":               self.tk_checkbox_bg,
                                 # "fg":               self.tk_checkbox_bg,    # not a known option win10
                                 # "activebackground": self.tk_checkboxbg,    # not a known option win10
                                }

        # ---- entry
        self.tk_entry_bg  = "white"
        self.tk_entry_fg  = "blue"
        self.tk_entry_ab  = "green"

        self.entry_dict   = {  "bg":               self.tk_entry_bg,
                                 # "fg":               self.tk_entry_bg,    # not a known option win10
                                 # "activebackground": self.tk_entry_bg,    # not a known option win10
                                }

        # ---- combobox   may need setting with ttk styles ......
        self.tk_combobox_bg  = "red"
        self.tk_combobox_fg  = "blue"
        self.tk_combobox_ab  = "green"

        self.combobox_dict   = {  'selectbackground':              self.tk_combobox_bg,
                                 # "fg":               self.tk_combobox_bg,    # not a known option win10
                                 # "activebackground": self.tk_combobox_bg,    # not a known option win10
                                }

        self.button_padding   = { "padx": 3, "pady": 3, }  # for use with palcement

    # ------------------------------------------
    def style_widget( self, a_widget ):
        """
        what it says   !! do we need ** seem to work without it
        returns:
            mutates the widget
        !! possibly use isinstance, but may need to be applied in just the right order for now use type
        !! change to dict type of dsipatch  ... started see self.style_dispacth_dict ... then put in dict use one function
        """
        widget_type = type( a_widget )

        if  widget_type  == Tk.Label:
            self.style_label( a_widget )
        # elif  widget_type == class( 'tkinter.Button' ):

        elif  widget_type == Tk.Button:
            #rint( "  type widget Tk.Button" )
            self.style_button( a_widget )

        elif  widget_type == Tk.Entry:
            #rint( "  type widget Tk.Entry" )
            self.style_entry( a_widget )

        elif  widget_type == Tk.Frame:
            #rint( "  type widget Tk.Entry" )
            self.style_frame( a_widget )

        elif  widget_type == Tk.Checkbutton:
            #rint( "  type widget Tk.Entry" )
            self.style_checkbutton( a_widget )

        elif  widget_type == Tk.ttk.Combobox:
            #rint( "  type widget Tk.Entry" )
            self.style_combobox( a_widget )

        else:
            print( "******************** no dispacth for this type widget" )
            print( f"style_widget --   str>>{widget_type}<<   repr >>{repr( a_widget)}<<" )

    # ------------------------------------------
    def style_button( self, a_widget ):
        """
        what it says   !! do we need ** seem to work without it
        returns:
            mutates the button
            add height ??
        """
        a_widget.config( self.button_dict )
        #print( "problem  with fg in styling look into " )

        # if True:    # old style without dict  -- configure or config
        #     pass
        #     # for testing without dict windows only green and red seem to ever show
        #     a_widget.configure( activebackground ='red' )
        #     a_widget.configure( activeforeground ='blue' )
        #     a_widget.configure( bg               ='green' )
        #     a_widget.configure( fg               ='yellow' )
        #     a_widget.configure( highlightcolor   ='orange' )


    # ------------------------------------------
    def style_frame( self, a_widget ):
        """
        what it says
        returns:
            mutates the frame
        """
        a_widget.config( self.frame_dict )

    # ------------------------------------------
    def style_tabp( self, a_widget ):
        """
        what it says
        returns:
            mutates the tab page
        """
        a_widget.config( self.tabp_dict )

    # ------------------------------------------
    def style_label( self, a_widget ):
        """
        what it says
        returns:
            mutates the label
        """
        a_widget.config( self.label_dict )

    # ------------------------------------------
    def style_checkbox( self, a_widget ):
        """
        what it says
        returns:
            mutates the label
        """
        a_widget.config( self.checkbox_dict )

    # ------------------------------------------
    def style_checkbutton( self, a_widget ):
        """
        what it says
        returns:
            mutates the label
        """
        a_widget.config( self.checkbutton_dict )

    # ------------------------------------------
    def style_combobox( self, a_widget ):
        """
        what it says
        returns:
            mutates the widget ... but combo boxes are special
        """
        print( "style_combobox implementation problems" )
        #a_widget.config( self.combobox_dict )

    # ------------------------------------------
    def style_entry( self, a_widget ):
        """
        what it says
        returns:
            mutates the widget = an entry
        """
        a_widget.config( self.entry_dict )

# --------------------------------------
class TitledFrame( object ):
    """
    new not tested
    About this class.....
    make a color coded frame ( two frames one above the other ) with a title in the top one and color coded
    see ex_tk_frame.py for the master
    """
    #----------- init -----------
    def __init__( self, a_parent_frame, a_title, a_title_color, button_width = 10,  button_height = 2 ):
        """
        Usual init see class doc string
        add to gui_ext !!
        """
        a_frame      = Tk.Frame( a_parent_frame,  bg ="red", )

        a_frame.rowconfigure(    0, weight= 1 )
        a_frame.rowconfigure(    1, weight= 1 )

        a_frame.columnconfigure( 0, weight= 1 )
        #master.columnconfigure( 1, weight= 1 )
        self.frame      = a_frame
        p_frame         = a_frame

        a_frame  = Tk.Frame( p_frame,   bg = a_title_color, )      # padx = 2, pady = 2, relief= Tk.GROOVE, )
        a_frame.grid( row = 0,  column = 0 ,sticky = Tk.E + Tk.W ) #+ Tk.N + Tk.S, )
        self.top_inner_frame    = a_frame

        a_label             = Tk.Label( a_frame, text = a_title, bg = a_title_color ,  ) #   relief = RAISED,  )
        a_label.grid( row = 0, column = 0, ) # columnspan = 1, sticky = Tk.W + Tk.E )

        a_frame  = Tk.Frame( p_frame,   ) # bg = "blue", )  # use neutral color or the title color
                                  # padx = 2, pady = 2, relief= Tk.GROOVE, )
        a_frame.grid( row = 1,  column = 0,sticky = Tk.E + Tk.W )   # + Tk.N + Tk.S, )
        self.bottom_inner_frame    = a_frame

        self.button_width  = button_width
        self.button_height = button_height
        self.button_row    = 0
        self.button_column = 0

        return



    #----------------------------------------------------------------------
    def make_button( self, button_text = "default text", command = None ):
        """
        !! have function make the button with the command
        """
        a_button = Tk.Button( self.bottom_inner_frame , width = self.button_width, height = self.button_height, text = button_text )
        a_button.grid( row  = self.button_row, column = self.button_column )
        self.button_column += 1

        if command is not None:
            a_button.config( command = command  )

        return a_button

# ---- if main  =================================================
if __name__ == "__main__":
    """
    possibly a test for both browses
    """

    # ------------------------------------------
    pass
    # testing = True


    # #test_listbox()
    # #test_dir_browse()

    # test_file_browse()










