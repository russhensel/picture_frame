# -*- coding: utf-8 -*-
#! /usr/bin/python3
#!python3
# above for windows ??
#     /usr/bin/python



"""
main class for picture_frame a picture display application

"""

import os
import logging
import sys
#import traceback
import importlib
# import pyperclip
#from   subprocess import Popen   #, PIPE  #
import time
import datetime
#from   pathlib import Path
import queue

import traceback

#----- local imports
import parameters
import gui
from   app_global import AppGlobal
#import file_filters
import helper_thread
import app_state
#import explore_args
#import db_objects




# ============================================
class App( object ):
    """
    this class is the "main" or controller for the whole app
    to run see end of this file
    it is the controller of an mvc app
     """
    def __init__( self, ):
        """
        usual init for main app
        """
        self.app_name          = "Picture Frame"
        self.app_version       = "Ver_1:  2021_10_16.01"
        self.version           = self.app_version        # hack till later
        AppGlobal.controller   = self
        self.gui               = None
        self.sort_list         = None   # create later may not be a list

        self.logger            = None   # for access in rest of class?
        AppGlobal.logger       = None

    # ----------------------------------
    def restart(self ):
        """
        use to restart the app without ending it
        this can be very quick
        """
        print( "========= restart file_finder =================" ) # not logged until it is turned on
        if not( self.gui is None ):
            self.gui.root.destroy()
            importlib.reload( parameters )    # should work on python 3 but sometimes does not

        else:
            #self.q_to_splash
            pass

        self.parameters     = parameters.Parameters( ) # open early as may effect other parts of code

        self.config_logger()
        self.prog_info()

       # could combine with above ??

        self.gui            = gui.GUI( self )

        msg       = "Error messages may be in log file, check it if problems -- check parmeters.py for logging level "
        print( msg )
        AppGlobal.print_debug( msg )
        self.logger.log( AppGlobal.fll, msg )

        self.app_state        = app_state.AppState()

        # set up queues before creating helper thread
        self.queue_to_helper    = queue.Queue( self.parameters.queue_length )   # send strings back to tkinker mainloop here
        self.queue_fr_helper    = queue.Queue( self.parameters.queue_length )
        #self.helper_thread      = smart_plug_helper.HelperThread( )
        self.helper_thread      = helper_thread.HelperThread()

        #AppGlobal.helper        = self.helper_thread  move to helper_thread
        self._polling_fail      = False   # flag set if _polling in gui thread fails

        self.helper_thread.start()

        self.polling_delta  = self.parameters.gt_poll_delta_t

        self.file_list      = []    # may need a clear function ( on line  )

        # self.read_file( "pictures_1.txt" )

        print( self.file_list )

        self.starting_dir   = os.getcwd()    # or perhaps parse out of command line

        # !! needs work
        # self.gui.root.iconify()
        # self.gui.root.update()
        # self.gui.root.deiconify()

        self.gui.root.after( self.polling_delta, self._polling_0_ )

#        print( "all done kernal error? return ")
#        return
        self.gui.root.mainloop()

        self.post_to_queue( "stop", None  , (  ) )

        self.helper_thread.join()
        self.logger.info( self.app_name + ": all done" )

    # ------------------------------------------
    def config_logger( self, ):
        """
        create/configure the python logger
        """
        AppGlobal.logger_id     = "App"        # or prerhaps self.__class__.__name__
        logger                  = logging.getLogger( AppGlobal.logger_id )
        logger.handlers         = []

        logger.setLevel( self.parameters.logging_level )

        # create the logging file handler
        fh = logging.FileHandler( self.parameters.pylogging_fn )

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)

        # add handler to logger object -- want only one add may be a problem
        logger.addHandler(fh)

        logger.info( "Done config_logger .. next AppGlobal msg" )
        #rint( "configed logger", flush = True )
        self.logger      = logger   # for access in rest of class?
        AppGlobal.logger = logger

        msg  = f"Message from AppGlobal.print_debug >> logger level in App = {self.logger.level} will show at level 10"
        AppGlobal.print_debug( msg )

    # --------------------------------------------
    def prog_info( self,  ):

        """
        ?? consider gui display or with button
        """
        #logger_level( "util_foo.prog_info"  )
        fll         = AppGlobal.force_log_level
        logger      = self.logger
        logger.log( fll, "" )
        logger.log( fll, "============================" )
        logger.log( fll, "" )
        title       =   f"Application: {self.app_name} {AppGlobal.parameters.mode}  {self.app_version}"
        logger.log( fll, title )
        logger.log( fll, "" )

        if len( sys.argv ) == 0:
            logger.info( "no command line arg " )

        else:
            for ix_arg, i_arg in enumerate( sys.argv ):
                msg = f"command line arg + {str( ix_arg ) }  =  { i_arg })"
                logger.log( AppGlobal.force_log_level, msg )

        logger.log( fll, f"current directory {os.getcwd()}"  )

        start_ts     = time.time()
        dt_obj       = datetime.datetime.utcfromtimestamp( start_ts )
        string_rep   = dt_obj.strftime('%Y-%m-%d %H:%M:%S')
        logger.log( fll, "Time now: " + string_rep )
        # logger_level( "Parameters say log to: " + self.parameters.pylogging_fn ) parameters and controller not available can ge fro logger_level

        return


            # next is alternative syntax for above

            #fi le_as_list = a_file.readlines()   # or itterable

# ---------------------- Helper Methods

    # ----------------------------------
    def _polling_0_( self,  ):
        """
        polling for application gui thread gt
        this is for the first itteration
        """
        pass
        self._polling_()

    # ----------------------------------
    def _polling_( self,  ):
        """
        polling for application gui thread gt
        this is for the first itteration
        """
        gt_poll_delta_t   = self.parameters.gt_poll_delta_t
        try:   # this is for talking with the second thread
            ( action, function, function_args ) = self._rec_from_queue_()
            while action != "":
                if action == "call":
                    #rint( "controller making call" )
                    sys.stdout.flush()
                    function( *function_args )
                elif action == "rec":
                    self.gui.print_rec_string(  function_args[ 0 ] )
                elif action == "send":
                    # but where is it actually sent ??
                    self.gui.print_send_string( function_args[ 0 ] )
                elif action == "info":
                    self.gui.print_info_string( function_args[ 0 ] )

                ( action, function, function_args ) = self._rec_from_queue_()

        except Exception as ex_arg:
            self.logger.error( "_polling Exception in file_finder: " + str( ex_arg ) )
            # ?? need to look at which we catch maybe just rsh
            (atype, avalue, atraceback)   = sys.exc_info()
            a_join  = "".join( traceback.format_list ( traceback.extract_tb( atraceback ) ) )
            self.logger.error( a_join )
            raise    # comment out

        finally:
            if  self._polling_fail:
                pass
            else:
                self.gui.root.after( gt_poll_delta_t, self._polling_ )  # reschedule event
                #self.graph_live.polling_mt()
        #rint("helper polling")
        #ime.sleep( 1 )             # debug
        return

  # --------------------------------------------------
    def post_to_queue( self, action, function, args ):
        """
        self.post_to_queue( action, function, args )
        request action by other thread
        """
        loop_flag          = True
        ix_queue_max       = 10
        ix_queue           = 0
        while loop_flag:
            loop_flag      = False
            ix_queue  += 1
            try:
                #rint( f"try posting {( action, function, args )}" )
                self.queue_to_helper.put_nowait( ( action, function, args ) )
            except queue.Full:

                # try again but give _polling a chance to catch up
                msg   = "smart_plug queue to ht full looping"
                print(  msg )
                self.logger.error( msg )
                # protect against infinite loop if queue is not emptied
                if self.ix_queue > ix_queue_max:
                    msg   = f"too much queue looping self.parameters.queue_sleep {self.parameters.queue_sleep}"
                    print( msg )
                    self.logger.error( msg )
                    pass
                else:
                    loop_flag   = True
                    time.sleep( self.parameters.queue_sleep )

    # --------------------------------------------------
    def _rec_from_queue_( self, ):
        """
        take an item off the queue, think here for expansion may not be currently used.
        ( action, function, function_args ) = self._rec_from_queue_()
        """
        try:
            action, function, function_args   = self.queue_fr_helper.get_nowait()
        except queue.Empty:
            action          = ""
            function        = None
            function_args   = None

        return ( action, function, function_args )

    #----------------- bcb's ----------------------
     # ----------------------------------
    def bcb_show_pictures( self,  ):
        """
        what is says, read
        """
        self.gui.do_clear_button( None )
        msg   = "---- Show Pictures ----"
        self.display_in_gui( msg )

        self.gui.enable_ht_buttons( False )

        a_function   = self.helper_thread.show_pictures   # ?? add argument file nem
        self.post_to_queue( "call", a_function, ( "",  ) )

    # ----------------------------------------------
    def bcb_stop( self,  ):
        """
        used as callback from gui button
        stop ht if running
        """
        #rint( "bcb_stop" )
        self.app_state.cancel_flag  = True

    # ----------------------------------------------
    def bcb_pause( self,  ):
        """
        used as callback from gui button
        pause the ht
        """
        #rint( "bcb_pause" )
        if self.app_state.pause_flag:
            new_state = False
            bn_text   = "Pause"
        else:
            new_state = True
            bn_text   = "Continue"

        self.app_state.pause_flag  = new_state
        self.gui.widget_pause.config( text  = bn_text  )

    # ----------------------------------------------
    def bcb_show_setup( self,  ):
        """
        used as callback from gui button
        what it says, read code
        """
        #rint( "bcb_show_setup" )

        self.gui.do_clear_button( None )
        msg   = "---- Show Setup ----"
        self.display_in_gui( msg )

        # -----
        msg   = f"\nMode:  {self.parameters.mode}"
        msg   = f"{msg}\nPicture file:  {self.parameters.picture_file_list}"

        msg   = f"{msg}\nPicture file:       {self.parameters.picture_file_list}"

        msg   = f"{msg}\npicture_sleep:      {self.parameters.picture_sleep}"
        msg   = f"{msg}\npicture_ran_bias:   {self.parameters.picture_ran_bias}"
        msg   = f"{msg}\npicture_ran_width:  {self.parameters.picture_ran_width}"

        msg   = f"{msg}\nComputername:  {self.parameters.computername}"

        self.display_in_gui( msg )

        # self.display_list_in_gui( self.parameters.dup_dir_list  )
        # self.display_in_gui(  f"    Dup Directory depth -> {self.parameters.dup_dir_depth}" )

        self.display_in_gui(  f"\nDebug Level -> {self.parameters.logging_level}" )

        msg   = "\n---- Show Setup Done----"
        self.display_in_gui( msg )

   # ----------------------------------------------
    def display_list_in_gui( self, a_list ):
        """
        so short inline it ??
        """
        for i_line in a_list:
            self.display_in_gui( f"    {i_line}"  )

    # ----------------------------------------------
    def bcb_test( self,  ):
        """
        used as callback from gui button
        test method only in development
        """
        print( "bcb_test" )


        self.gui.enable_ht_buttons( False )

    # ----------------------------------------------
    def ht_state_change( self, is_busy  ):
        """
        used as callback from gui button resolve deleted methods
        """
        if is_busy:
            pass #
            print( "ht_state_change -- may need code ")  # ??
        else:
            print( "ht_state_change -- enable true, is busy false  ")
            self.gui.enable_ht_buttons( True )

    # ----------------------------------------------
    def null_method( self,  ):
        """
        used as callback from gui button resolve deleted methods
        """
        #rint("call to null_method")

    # ----------------------------------------------
    def os_open_logfile( self,  ):
        """
        used as callback from gui button
        what it says read
        """
        AppGlobal.os_open_txt_file( self.parameters.pylogging_fn )

    # ----------------------------------------------
    def os_open_parmfile( self,  ):
        """
        used as callback from gui button
        what it says read
        """
        a_filename = self.starting_dir  + os.path.sep + "parameters.py"
        AppGlobal.os_open_txt_file( a_filename )

    # ----------------------------------------------
    def os_open_help( self,  ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_help_file( self.parameters.help_fn )

    # ----------------------------------------------
    def display_in_gui( self, a_string ):
        """
        what it says, read
        for prompt display need to have multi threading
        """
        self.gui.write_gui( a_string )

# ----------------------------------------------
def main():
    a_app = App( )
    #sg   = "init done but no logger"
    #rint( msg )
    if True:
        a_app.restart()
    else:

        try:
            a_app.restart()
        except Exception as exception:
            if a_app.logger == None:
                msg   = "exception in __main__ run the app -- it will end -- logger still None "
                print( msg )
            else:
                msg   = "exception in __main__ run the app -- it will end -- see logger "
                print( msg )
                a_app.logger.critical( msg )

                a_app.logger.critical( exception,  stack_info=True )   # just where I am full trace back most info
                raise
        finally:
            print( "here we are done with file_finder ", flush = True )
    #        sys.stdout.flush()

# ------------------
# ==============================================
if __name__ == '__main__':
    """
    run the app here, for convenience of launching
    """
    main(  )

# ======================= eof =======================

