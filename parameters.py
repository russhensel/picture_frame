# -"*- codin"g: utf-8" -*-

"""
parameters for file_finder



"""

import logging
import sys
import os


# ------ local ------

#import file_filters
from   app_global import AppGlobal
# import file_filters

#-------------------------------------------
class Parameters( object ):
    """
    manages parameter values use like ini file
    a struct but with a few smarts
    """
    def __init__(self,   ):

        self.controller        = AppGlobal.controller    # save a link to the controller not actually used now
        AppGlobal.parameters   = self

        self.file_filter_function  = None

        # ---------  os platform... ----------------
        self.default_config()
        self.os_tweaks( )
        self.computer_name_tweaks()

        # -------------- choose ---------
#        self.russ_1_mode()
        #self.mode_test_1()
#        self.test_photo_db_1()

        self.mode_test_for_dev_1()
        #self.test_photo_db_1()
        return

# ------->> Subroutines:  one for each mode

    # -------
    def mode_test_for_dev_1( self ):
        """
        a test for use in development
        """
        self.mode               = "mode_test_for_dev_1"

        # --------------------- dup finding related

        # add check to make sure all are directories


        self.logging_level               = logging.INFO   #  DEBUG  iNFO 20

        # msg    = f"filter info {self.file_filter_object}"
        # print( msg )  # but this is already in gui

        self.picture_file_list  = r"file_name_b.txt"


    # -------
    def mode_russ_1_modexxx( self ):
        """
        first mode for smithers
        untested, prob bad
        """
        self.mode               = "mode_russ_1_mode"

        self.logging_level      = logging.DEBUG


# ----------------- tweaks changes for all modes for an os or computer name, to be used with other modes
    # -------
    def os_tweaks( self ):
        """
        tweak the default settings of "default_ _mode"
        for particular operating systems
        you may need to mess with this based on your os setup
        """
        if  self.os_win:
            pass
            #self.icon               = r"./clipboard_b.ico"    #  greenhouse this has issues on rasPi
            #self.icon              = None                    #  default gui icon

        else:
            pass

    # -------
    def computer_name_tweaks( self ):
        """
        this is an subroutine to tweak the default settings of "default_mode"
        for particular computers.  Put in settings for you computer if you wish
        these are for my computers, add what you want ( or nothing ) for your computes
        """
        print(self.computername, flush=True)

        if self.computername == "smithersxx":
            self.win_geometry       = '1250x1000+20+20'      # width x height position
            self.ex_editor          =  r"D:\apps\Notepad++\notepad++.exe"    # russ win 10 smithers

        elif self.computername == "millhousexx":
            self.ex_editor          =  r"C:\apps\Notepad++\notepad++.exe"    # russ win 10 millhouse
            self.win_geometry       = '1000x700+250+5'          # width x height position
#            self.pylogging_fn       = "millhouse_clipboard.py_log"   # file name for the python logging
#            self.snip_file_fn       = r"C:\Russ\0000\python00\python3\_projects\clipboard\Ver3\snips_file_auto.txt"
#            # need to associate with extension -- say a dict
#            self.snip_file_command  = r"C:\apps\Notepad++\notepad++.exe"  #russwin10   !! implement

    # -------
    def default_config( self ):

        self.mode              = "default_config"
        our_os                 = sys.platform
#        print( "our_os is ", our_os )

        #if our_os == "linux" or our_os == "linux2"  "darwin":

        if our_os == "win32":
            self.os_win = True     # right now windows and everything else
        else:
            self.os_win = False

        self.platform           = our_os    # sometimes it matters which os

        self.computername       = ( str( os.getenv( "COMPUTERNAME" ) ) ).lower()

        # ---- appearance
        self.title              = "File Finder for Dups"   # window title  !! drop for name version

        self.icon               = r"clipboard_c.ico"
        self.icon               = r"broom_edit_2.ico"    # do we need to convert to ico -- or down sample

        self.id_color           = "yellow"                # ?? not implementd

        self.win_geometry       = '1500x800+20+20'     # width x height position
        self.win_geometry       = '900x600+700+230'     # width x height position  x, y
        self.win_geometry       = '1400x1000+20+20'

        self.default_scroll     = True
        self.max_lines          = 2_000

        # ---- logging
        self.pylogging_fn       = "picture_frame.py_log"   # file name for the python logging
        self.logging_level      = logging.DEBUG        # logging level DEBUG will log all catured text !
        self.logging_level      = logging.INFO
        self.logger_id          = "file_finder"         # id in logging file

        # ------------- file names -------------------

        # this is the name of a program: its executable with path inof.
        # to be used in opening an external editor
        self.ex_editor         =  r"D:\apps\Notepad++\notepad++.exe"    # russ win 10

        #  example use as if self.controller.parameters.os_win:
        # ---- pictures
        self.picture_file_list  = r"pictures_1.txt"               # list of pictures to show
        self.picture_sleep      = 2  # time between pics
        self.picture_ran_bias   = 2
        self.picture_ran_width  = 20

        self.help_fn            = "help.txt"
        self.help_fn            = "https://opencircuits.com/index.php?title=Delete_Duplicates_Help_File"

        self.gui_text_log_fn    = r"gui_text.log"

        self.gui_text_log_fn    = False  # "gui_text.log"       # a file name or something false

        self.log_gui_text       = False # True or false to log text to normal log file
        self.log_gui_text_level = 10    # logging level for above

        # ---------------------thread related gt gui thread ht = helper thread  ------------------------------
        #self.queue_sleep         = 2          # time for backup to sleep if queue fills
        self.gt_queue_sleep      = 2          # time in seconds  -- this is new name aqueue_sleep is on its way  gt is gui therad
        #gt_poll_delta_t
        self.gt_poll_delta_t     = 200        # how often wee poll for clip changes, in ms ?
        self.queue_length        = 50
        self.ht_poll_delta_t     = .200        # how often wee poll for clip changes, in sec
        self.ht_queue_sleep      = 2          # time in seconds  -- this is new name aqueue_sleep is on its way  gt is gui therad
        self.ht_pause_sleep      = 1

        self.ix_queue_max        = 10         # number of times queue sleep is allowed to loop before error

        # ---------------------

# =================================================
if __name__ == "__main__":

    import  picture_frame
    picture_frame.main( )


# =================== eof ==============================





