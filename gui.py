# -*- coding: utf-8 -*-

#   gui for file_finder

import sys
import tkinter as Tk
from   PIL      import ImageTk, Image
import logging

import time

# ---- local
sys.path.append( "../rshlib" )
import gui_ext
#import gui_helper
from   app_global import AppGlobal



# this lets us get code from a directory
#sys.path.append( "D:/Russ/0000/SpyderP/DirectoryBackup" )



# --------------------------------------

class GUI( object ):

    def __init__( self, controller  ):

        AppGlobal.gui       = self
        self.controller     = controller
        self.parameters     = controller.parameters   # or from app global
        self.logger         = logging.getLogger( AppGlobal.logger_id + ".gui")
        #self.logger.info("in class GUI init for the clip_board_1") # logger not currently used by here
        self.root_image     = None
        # ----- start building gui
        self.root           = Tk.Tk()
        # self.root_image     = Tk.Tk()   # works but window quite different


        #self.root.title( self.controller.parameters.title )   f"Application: {self.app_name} {AppGlobal.parameters.mode}  {self.app_version}"
        self.root.title( f"{self.controller.app_name} mode: {AppGlobal.parameters.mode} / version: {self.controller.app_version}" )

        #        # ----- set up root for resizing
        #        self.root.grid_rowconfigure(    0, weight=0 )
        #        self.root.grid_columnconfigure( 0, weight=1 )
        #
        #        self.root.grid_rowconfigure(    1, weight=1 )
        #        self.root.grid_columnconfigure( 1, weight=1 )


        # ------------ message frame ---------------------   rec
        self.cb_scroll_var   = Tk.IntVar()  #
        self.max_lines       = self.parameters.max_lines

        parent_frame         = self.root

        parent_frame.rowconfigure(    0, weight= 0 )
        parent_frame.rowconfigure(    1, weight= 0 )

        parent_frame.columnconfigure( 0, weight= 1 )

        parent_frame.configure( background="grey" )

        ix_row   = -1  # increment to 0 later
        # ----- ._make_meta_widget_frame
        a_frame   = self._make_meta_widget_frame( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ----- _make_command_frame_
        a_frame   = self._make_command_frame_( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # was the id frame
#        a_frame   = Tk.Frame( self.root, width=300, height=20, bg = self.parameters.id_color , relief = Tk.RAISED, ) # borderwidth=1 )
#        ix_row   += 1
#        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ---- image frame
        # ix_row   += 1
        # frame0 = self._make_image_frame( self.root )
        # frame0.grid( row=ix_row, column=0, sticky=Tk.N + Tk.S + Tk.E + Tk.W )
        # self.root.grid_rowconfigure(    ix_row, weight=1 )

        # ----------- next frame
        ix_row   += 1
        frame0 = self._make_message_frame( self.root )
        frame0.grid( row=ix_row, column=0, sticky=Tk.N + Tk.S + Tk.E + Tk.W )
        self.root.grid_rowconfigure(    ix_row, weight=1 )

        # ------ end of frames

        # ---- image window xxxx
        #self.make_root_image()

        #self.root.grid_rowconfigure(    ix_row, weight=1 )

        self.root.geometry( self.controller.parameters.win_geometry )

        print( "next icon..." )
        # if self.parameters.os_win:
        #     # icon may cause problem in linux for now only use in win
        #     icon_fn = self.parameters.icon
        #     print( "in windows setting icon {icon_fn}" )
        #     self.root.iconbitmap( icon_fn )

        if self.parameters.os_win:
            # icon may cause problem in linux for now only use in win
#            Changing the application and taskbar icon - Python/Tkinter - Stack Overflow
#            https://stackoverflow.com/questions/14900510/changing-the-application-and-taskbar-icon-python-tkinter
            import ctypes
            myappid = self.parameters.icon # arbitrary string
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
#            print( "in windows setting icon" )
            self.root.iconbitmap( self.parameters.icon )
        msg       = "... icon done...."
        print( msg )

    # ------------------------------------------
    def _make_meta_widget_frame( self, parent_frame,  ):
        """
        this contains a button area for app control stuff
        passed a parent
        returns this main_frame
        """
        tframe_1      = TitledFrame( parent_frame, "System Operations:", self.parameters.id_color )
        a_frame       = tframe_1.frame

        a_button = tframe_1.make_button( button_text = "Ed Log", command = self.controller.os_open_logfile  )
        #a_button.config( command = self.controller.os_open_logfile  )

        a_button = tframe_1.make_button( button_text =  "Ed Parms" )
        a_button.config( command = self.controller.os_open_parmfile  )

        a_button = tframe_1.make_button( button_text =  "Ed Log" )
        a_button.config( command = self.controller.os_open_logfile  )

        a_button = tframe_1.make_button( button_text =  "Ed Parms" )
        a_button.config( command = self.controller.os_open_parmfile  )

        a_button = tframe_1.make_button( button_text =  "Restart" )
        a_button.config( command = self.controller.restart  )

        a_button = tframe_1.make_button( button_text =  "Help" )
        a_button.config( command = self.controller.os_open_help  )

        a_button = tframe_1.make_button( button_text =  "About" )
        a_button.config( command = AppGlobal.about  )

        return a_frame

    # ------------------------------------------
    def _make_command_frame_( self, parent_frame,  ):
        """
        buttons for commands that operate on files
        returns this frame
        !! have function make the button with the command
        """
        tframe_1      = TitledFrame( parent_frame, "Picture:", self.parameters.id_color )
        a_frame       = tframe_1.frame

        a_widget = tframe_1.make_button( button_text = "0-Show\nSetup" )
        a_widget.config( command = self.controller.bcb_show_setup  )
        self.widget_define_db   = a_widget

        a_widget = tframe_1.make_button( button_text = "Show Pictures" )
        a_widget.config( command = self.controller.bcb_show_pictures  )
        self.widget_define_db   = a_widget

        # a_widget = tframe_1.make_button( button_text = "2-Explore\nDups" )
        # a_widget.config( command = self.controller.bcb_explore_dups  )
        # self.widget_explore_dups  = a_widget

        # a_widget = tframe_1.make_button( button_text = "3-Explore\nKeeps" )
        # a_widget.config( command = self.controller.bcb_explore_keeps )
        # self.widget_explore_keeps  = a_widget

        a_widget = tframe_1.make_button( button_text = "Pause" )
        a_widget.config( command = self.controller.bcb_pause )
        self.widget_pause  = a_widget

        a_widget = tframe_1.make_button( button_text = "Stop" )
        a_widget.config( command = self.controller.bcb_stop )
        self.widget_stop  = a_widget

        a_widget = tframe_1.make_button( button_text = "Test" )
        a_widget.config( command = self.controller.bcb_test  )

        return a_frame


    # ------------------------------------------
    def _on_close_root_image( self,   ):
        """
        what it says, read

        """
        print( "_on_close_root_image" )

        self.controller.bcb_stop()
        self.root_image.destroy()
        self.root_image  = None

    # ------------------------------------------
    def make_root_image( self,   ):
        """
        what it says, read

        """
        if not self.root_image is None:  # should not happen, maybe this is a safety thing ??
            return

        self.root_image     = Tk.Toplevel( self.root )
        self.root_image.geometry( '500x500+500+500'  )
        self.root_image.protocol( "WM_DELETE_WINDOW", self._on_close_root_image )  # search for function def

        # ---- image frame 2
        #ix_row   += 1
        frame0     = self._make_image_frame( self.root_image )
        frame0.grid( row=0, column=0, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        self.root_image.grid_rowconfigure(       0, weight=1 )
        self.root_image.grid_columnconfigure(    0, weight=1 )

        return

    # ------------------------------------------
    def _make_image_frame( self, parent,  ):
        """
        what it says, read
        """
        a_frame              = Tk.Frame( parent )
        a_frame.grid_rowconfigure(    0, weight=1 )
        a_frame.grid_columnconfigure( 0, weight=1 )

        a_widget             = Tk.Label( a_frame, text = "self.image_label", bg = "gray", width = 200, height = 200 )

        a_widget.grid( row  = 0, column = 0, sticky = Tk.E + Tk.W  + Tk.N + Tk.S, )
        self.image_label     = a_widget

        # self.display_image( r"D:\PhotoDB\13\DSC02114.JPG" )

        return a_frame

    # ------------------------------------------
    def _make_message_frame( self, parent,  ):
        """
        make the message frame for user feedback
        """
        a_frame              = gui_ext.MessageFrame( parent,  )
        self.message_frame   = a_frame
        return a_frame




    # # ------------------------------------------
    # def display_image_list( self, parent,  ):
    #     """
    #     what it says
    #     not used but might move some code l
    #     """
    #     for i_file in self.controller.file_list:
    #         self.display_image( i_file )
    #         time.sleep( 2 )

    # ------------------------------------------
    def display_image( self, file_name, title_text = None  ):
        """
        what it says, read
        """
        if title_text is None:
            title_text = file_name

        self.root_image.title( title_text )

        width_screen    = self.root_image.winfo_screenwidth()
        height_screen   = self.root_image.winfo_screenheight()

        width_screen    = self.root_image.winfo_width()
        height_screen   = self.root_image.winfo_height()

        image           = Image.open( file_name )
        width, height   = image.size

        width_factor    = width_screen / width

        if width_factor * height <= height_screen:
            # use width factor
            width           = int( width  * width_factor )
            height          = int( height * width_factor )
        else:
            height_factor   = height_screen / height
            width           = int( width  * height_factor )
            height          = int( height * height_factor )

        #rint( f"original image size {width} x {height}")

        resize_image   = image.resize((width, height))
        img            = ImageTk.PhotoImage( resize_image )

        # display
        self.image_label.configure( image = img )
        self.image_label.image            = img

    # ==================== end construction ====================
    # construction helpers

    def _make_titled_listbox_( self, parent_frame, a_title ):
        """
        return ( famelike_thing, listbox_thing)  ?? make a class, better acces to components
        """
        a_frame      = Tk.Frame(parent_frame)
        a_listbox    = Tk.Listbox( a_frame, height=5 )
        a_listbox.grid( column=0, row=1, sticky=(Tk.N, Tk.W, Tk.E, Tk.S) )
        s = Tk.Scrollbar( a_frame, orient=Tk.VERTICAL, command=a_listbox.yview)
        s.grid(column=1, row=1, sticky=(Tk.N, Tk.S))
        a_listbox['yscrollcommand'] = s.set
        a_label = Tk.Label( a_frame, text= a_title )
        a_label.grid( column=0, row=0, sticky=( Tk.N, Tk.E, Tk.W) )
        #  ttk.Sizegrip().grid(column=1, row=1, sticky=(Tk.S, Tk.E)) size grip not appropriate here
        a_frame.grid_columnconfigure( 0, weight=1 )
        a_frame.grid_rowconfigure(    0, weight=0 )
        a_frame.grid_rowconfigure(    1, weight=1 )
        return ( a_frame, a_listbox )

    #----------------------------------------------------------------------
    def write_gui_wt(self, title, a_string ):
        """
        write to gui with a title.
        title     the title
        a_string  additional stuff to write
        make a better function with title = ""  ??
        title the string with some extra formatting
        clear and write string to input area
        """
        self.write_gui( " =============== " + title  + " ==> \n" +  a_string )   # better format or join ??

    #----------------------------------------------------------------------
    def write_gui( self, string ):
        """
        clear and write string to input area
        leave disabled
        """
        self.message_frame.display_string( string )



#         self.text_in['state'] = 'normal'      # normal  disabled
# #        self.text_in.delete( 1.0, Tk.END )
#         self.text_in.insert( Tk.END, f"{string}\n" )
#         self.text_in['state'] = 'disabled'      # normal  disabled
#         # self.text_in.see( Tk.END )  # add scrolling

#         # self.text_in.delete( 1.0, str( cut ) + ".0" )

#         try:
#              numlines = int( self.text_in.index( 'end - 1 line' ).split('.')[0] )  # !! beware int( None ) how could it happen ?? it did this is new
#         except Exception as exception:
#         # Catch the custom exception
#             self.logger.error( str( exception ) )
#             print( exception )
#             numlines = 0



#         # manage too many lines
#         if numlines > self.max_lines:
#             cut  = int( numlines/2 )    # lines to keep/remove
#             # remove excess text
#             self.text_in.delete( 1.0, str( cut ) + ".0" )
# #            msg     = "Delete from test area at " + str( cut )
# #            self.logger.info( msg )
#         # manage autoscroll
#         if self.cb_scroll_var.get():
#             self.text_in.see( Tk.END )

#         return


    # ------------------------------------------
    def enable_ht_buttons( self, enable_if_true ):
        """
        for the clear button
        clear the text area
        even can be anything
        enable_if_true       True enable else disable
        """
        if enable_if_true:
            new_state  = Tk.NORMAL
            #rint( f"enable_ht_buttons Tk.NORMAL" )
        else:
            new_state  = Tk.DISABLED
            #rint( f"enable_ht_buttons Tk.DISABLED" )

        self.widget_define_db.config(     state = new_state )
        #self.widget_explore_keeps.config( state = new_state )
        #self.widget_explore_dups.config(  state = new_state )

    # ------------------------------------------
    def do_clear_button( self, event):
        """
        for the clear button
        clear the text area
        even can be anything
        """
        self.message_frame.do_clear_button( event )


    # ------------------------------------------
    def do_copy_button( self, event ):
        """
        copy all text to the clipboard
        """
        self.message_frame.do_copy_button()

    # ------------------------------------------
    def do_auto_scroll( self,  ):
        """
        pass, not needed, place holder
        """
        print( "do_auto_scroll needs completion" )
        # not going to involve controller
        pass
        return

#----------------------------------------------------------------------
class RedirectText(object):
    """
    simple class to let us redirect console prints to our recieve area
    http://www.blog.pythonlibrary.org/2014/07/14/tkinter-redirecting-stdout-stderr/
    """
    #----------------------------------------------------------------------
    def __init__(self, text_ctrl):
        """Constructor
        text_ctrl text area where we want output to go
        """
        self.output = text_ctrl

    #----------------------------------------------------------------------
    def write(self, string):
        """
        """
        self.output.insert( Tk.END, string )
        # add scrolling?
        self.output.see( Tk.END )

        #self.rec_text.insert( END, adata, )
        #self.rec_text.see( END )

    #--------------------------------
    def flush(self, ):
        """
        here to mimic the standard sysout
        does not really do the flush
        """
        pass

# --------------------------------------
class TitledFrame( object ):
    """
    About this class.....
    make a color coded frame ( two frames one above the other ) with a title in the top one and color coded
    see ex_tk_frame.py for the master
    """
    #----------- init -----------
    def __init__( self, a_parent_frame, a_title, a_title_color, button_width = 10,  button_height = 2 ):
        """
        Usual init see class doc string
        add to gui_ext !!
        """
        a_frame      = Tk.Frame( a_parent_frame,  bg ="red", )

        a_frame.rowconfigure(    0, weight= 1 )
        a_frame.rowconfigure(    1, weight= 1 )

        a_frame.columnconfigure( 0, weight= 1 )
        #master.columnconfigure( 1, weight= 1 )
        self.frame      = a_frame
        p_frame         = a_frame

        a_frame  = Tk.Frame( p_frame,   bg = a_title_color, )      # padx = 2, pady = 2, relief= Tk.GROOVE, )
        a_frame.grid( row = 0,  column = 0 ,sticky = Tk.E + Tk.W ) #+ Tk.N + Tk.S, )
        self.top_inner_frame    = a_frame

        a_label             = Tk.Label( a_frame, text = a_title, bg = a_title_color ,  ) #   relief = RAISED,  )
        a_label.grid( row = 0, column = 0, ) # columnspan = 1, sticky = Tk.W + Tk.E )

        a_frame  = Tk.Frame( p_frame,   ) # bg = "blue", )  # use neutral color or the title color
                                  # padx = 2, pady = 2, relief= Tk.GROOVE, )
        a_frame.grid( row = 1,  column = 0,sticky = Tk.E + Tk.W )   # + Tk.N + Tk.S, )
        self.bottom_inner_frame    = a_frame

        self.button_width  = button_width
        self.button_height = button_height
        self.button_row    = 0
        self.button_column = 0

        return


    #----------------------------------------------------------------------
    def make_button( self, button_text = "default text", command = None ):
        """
        !! have function make the button with the command
        """
        a_button = Tk.Button( self.bottom_inner_frame , width = self.button_width, height = self.button_height, text = button_text )
        a_button.grid( row  = self.button_row, column = self.button_column )
        self.button_column += 1

        if command is not None:
            a_button.config( command = command  )

        return a_button


    #---------------------
def maximize( top_level ):
        """

        maybe move into obect and   maximize_root  maximize_image

        """
        if AppGlobal.parameters.os_win:

            #self.root.wm_attributes( '-zoomed', True )
            # self.root.attributes( '-zoomed', True )
            #self.root.attributes( '-zoomed',)
            #self.root.state('zoomed')   # maybe just windows where it does seem to work
            # https://www.tcl.tk/man/tcl/TkCmd/wm.htm#m8   # also google python wm state zoomed

            #he most pythonic is" root.wm_state('zoomed'), as mentioned by @J.F.Sebastian
            #To show                                                            d window with title bar use the 'zoomed' attribute

            # self.root.wm_state('zoomed')  no error on windows but is it a help
            # self.root.attributes('-zoomed', True)  # error in windows

            top_level.state('zoomed')   # maybe just windows where it does seem to work
        else: # as close as I can get so far for linux
                #w = self.root.winfo_screenwidth()
                #h = self.root.winfo_screenheight()
                top_level.geometry("%dx%d+0+0" % (top_level.winfo_screenwidth(), top_level.winfo_screenheight() ))



# =================================================

if __name__ == "__main__":

    import   picture_frame
    picture_frame.main( )


# =================== eof ==============================